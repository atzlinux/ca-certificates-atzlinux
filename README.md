show atzlinux CA info:

```bash
openssl x509 --in atzlinux-uefi-ca.der -text
```

or

```bash
- apt install certinfo
- openssl x509 -inform DER  -in atzlinux-uefi-ca.der -outform PEM -out atzlinux-uefi-ca.crt
- certinfo atzlinux-uefi-ca.crt
```

atzlinux's grub-efi-amd64-signed package will install atzlinux-uefi-ca.der to:

/boot/efi/atzlinux-uefi-ca.der
